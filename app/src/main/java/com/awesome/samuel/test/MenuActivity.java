package com.awesome.samuel.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        final List<Integer> imgList = new ArrayList<>();
        imgList.add(R.drawable.food1);
        imgList.add(R.drawable.food2);
        imgList.add(R.drawable.food3);
        final List<String> itemList = new ArrayList<>();
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");
        itemList.add("Fried Rice");
        itemList.add("Spegt");
        itemList.add("Egg");
        itemList.add("Omelets");
        itemList.add("Noodles");

        ListView menuList = findViewById(R.id.menu_list);
        ArrayAdapter arrayAdapter = new ArrayAdapter(MenuActivity.this, android.R.layout.simple_list_item_1, itemList);
        menuList.setAdapter(arrayAdapter);
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MenuActivity.this, ItemActivity.class);
                intent.putExtra(ItemActivity.itemNameKey, itemList.get(i));
                intent.putExtra(ItemActivity.itemImgKey, imgList.get(i));
                startActivity(intent);
            }
        });
    }
}
