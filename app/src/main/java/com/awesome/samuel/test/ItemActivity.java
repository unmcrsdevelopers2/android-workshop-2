package com.awesome.samuel.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemActivity extends AppCompatActivity {

    public static String itemNameKey = "itemNameKey";
    public static String itemImgKey = "itemImgKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        String itemName = getIntent().getStringExtra(itemNameKey);
        int itemImg = getIntent().getIntExtra(itemImgKey, 0);

        TextView itemNameTV = findViewById(R.id.item_name);
        ImageView itemImgIV = findViewById(R.id.item_img);
        itemImgIV.setImageResource(itemImg);
        itemNameTV.setText(itemName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(ItemActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
